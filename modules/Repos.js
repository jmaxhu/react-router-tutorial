import React from 'react'
import { Router, Link, browserHistory } from 'react-router'
import NavLink from './NavLink'

export default React.createClass({
  handleSubmit (e){
    e.preventDefault()

    const userName = e.target.elements[0].value
    const repoName = e.target.elements[1].value
    const path = `/repos/${userName}/${repoName}`
    console.log(path)
    browserHistory.push(path)
  },
  render () {
    return (<div>
      <h2>Repos</h2>
        <ul>
          <li><NavLink to="/repos/rackt/react-router">React Router</NavLink></li>
          <li><NavLink to="/repos/facebook/react">React</NavLink></li>
          <li>
            <form onSubmit={this.handleSubmit}>
              <input type="text" name="userName" /> / { ' '}
              <input type="text" name="repoName" />
              <button type="submit">Go</button>
            </form>
          </li>
        </ul>

      { this.props.children }
      </div>)
  }
})
